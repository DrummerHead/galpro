/** @jsx preact.h */
// @flow

import preact from 'preact';

import type { MediaProps } from '../main.js';

type VideoProps = MediaProps & {
  volume: number,
};
type VideoState = {
  isPlaying: boolean,
  playbackStep: number,
};
class Video extends preact.Component<VideoProps, VideoState> {
  // By how many seconds fast-forward or rewind
  timeDelta: number = 5;
  // Intermediate playback speed multipliers
  playBackSteps: Array<number> = [0.1, 0.25, 0.5, 0.75, 1, 1.5, 2, 4, 6, 10];
  // Position of normal playback speed in `playBackSteps`
  playBackStepRegular: number = 4;
  videoDom: ?HTMLVideoElement;

  state = {
    isPlaying: false,
    playbackStep: this.playBackStepRegular,
  };

  managePlay() {
    if (this.videoDom) {
      this.state.isPlaying ? this.videoDom.play() : this.videoDom.pause();
    }
  }

  seek(n: number) {
    if (this.videoDom) {
      this.videoDom.currentTime += n;
    }
  }

  toggle() {
    this.setState(prevState => ({
      isPlaying: !prevState.isPlaying,
    }));
    this.managePlay();
  }

  handlePlaybackRate({
    faster = false,
    slower = false,
    reset = false,
  }: {
    faster?: boolean,
    slower?: boolean,
    reset?: boolean,
  }) {
    if (faster && this.state.playbackStep < this.playBackSteps.length - 1) {
      this.setState(prevState => ({
        playbackStep: prevState.playbackStep + 1,
      }));
    } else if (slower && this.state.playbackStep > 0) {
      this.setState(prevState => ({
        playbackStep: prevState.playbackStep - 1,
      }));
    } else if (reset) {
      this.setState({
        playbackStep: this.playBackStepRegular,
      });
    }
    if (this.videoDom) {
      this.videoDom.playbackRate = this.playBackSteps[this.state.playbackStep];
    }
  }

  keyPressHandler = (ev: KeyboardEvent) => {
    if (this.props.isCurrent) {
      switch (ev.key) {
        // Toggle play/pause
        case 'u':
          this.toggle();
          break;
        // Rewind `timeDelta` seconds
        case 'i':
          this.seek(-this.timeDelta);
          break;
        // Fast-forward `timeDelta` seconds
        case 'o':
          this.seek(this.timeDelta);
          break;
        // Slower motion
        case 'n':
          this.handlePlaybackRate({ slower: true });
          break;
        // Faster motion
        case 'm':
          this.handlePlaybackRate({ faster: true });
          break;
        // Reset playback speed
        case ',':
          this.handlePlaybackRate({ reset: true });
          break;
      }
    }
  };

  componentDidMount() {
    this.setState({
      isPlaying: this.props.isCurrent,
    });
    this.managePlay();
    document.addEventListener('keypress', this.keyPressHandler);
  }

  componentWillUnmount() {
    this.videoDom = undefined;
    document.removeEventListener('keypress', this.keyPressHandler);
  }

  componentWillReceiveProps(nextProps: VideoProps) {
    if (this.props.isCurrent !== nextProps.isCurrent) {
      this.setState({
        isPlaying: nextProps.isCurrent,
      });
      this.managePlay();
    }
  }

  widthStyle = () =>
    this.props.isCurrent ? {} : { width: `${this.props.width}vw` };

  render() {
    return (
      <div
        className={`picContain video ${
          this.props.isCurrent ? 'currentPic' : 'cachePic'
        }`}
        data-id={this.props.id}
        style={this.widthStyle()}
      >
        <video
          src={this.props.href}
          className="pic"
          controls={this.props.isCurrent}
          loop
          preload="auto"
          volume={this.props.volume}
          onCanPlayThrough={this.props.handleLoaded(this.props.id)}
          onError={this.props.handleError(this.props.id)}
          ref={elem => (this.videoDom = elem)}
        />
        <div
          className="videoGlass"
          onClick={this.props.onClick(this.props.id)}
        />
      </div>
    );
  }
}

export default Video;
