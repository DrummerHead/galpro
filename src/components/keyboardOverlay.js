/** @jsx preact.h */
// @flow

import preact from 'preact';

export type Keys = Array<{
  padLeft: number,
  keys: Array<[string, string]>,
}>;
type KeyList = Array<string>;

const makeKeyList = (keys: Keys): KeyList =>
  keys.reduce(
    (
      acc0: KeyList,
      curr0: { padLeft: number, keys: Array<[string, string]> }
    ) => [
      ...acc0,
      ...curr0.keys.reduce(
        (acc1: KeyList, curr1: [string, string]): KeyList => [
          ...acc1,
          curr1[0],
        ],
        []
      ),
    ],
    []
  );

const keyWidth = 60;
const stagger = 12;

const keyRowMarginLeft = (i: number, padLeft: number): number =>
  i * stagger + padLeft * keyWidth;

const keyboardOverlayWidth = (keys: Keys): number =>
  Math.max(
    ...keys.map(
      (keyRow, i) =>
        keyRowMarginLeft(i, keyRow.padLeft) + keyRow.keys.length * keyWidth
    )
  );

type KeyboardOverlayProps = {
  keys: Keys,
  position: {
    top?: string,
    right?: string,
    bottom?: string,
    left?: string,
  },
};
type KeyboardOverlayState = {};

class KeyboardOverlay extends preact.Component<
  KeyboardOverlayProps,
  KeyboardOverlayState
> {
  keyEvents: { [string]: KeyboardEvent } = {};
  keyList: KeyList = [];

  newKeyboardEvent = (key: string) => {
    this.keyEvents[key] = new KeyboardEvent('keydown', { key });
  };

  componentDidMount() {
    this.keyList = makeKeyList(this.props.keys);
    this.keyList.forEach(this.newKeyboardEvent);
  }

  shouldComponentUpdate(nextProps: KeyboardOverlayProps): boolean {
    if (nextProps !== this.props) {
      this.keyList = makeKeyList(nextProps.keys);
      this.keyList.forEach(this.newKeyboardEvent);
    }
    return nextProps !== this.props;
  }

  emulateKeydown = (key: string) => () => {
    document.dispatchEvent(this.keyEvents[key]);
  };

  render() {
    return (
      <ul
        class="keyboardOverlay"
        style={{
          ...this.props.position,
          width: keyboardOverlayWidth(this.props.keys),
        }}
      >
        {this.props.keys.map((keyRow, i) => (
          <li style={{ marginLeft: keyRowMarginLeft(i, keyRow.padLeft) }}>
            <ul class="keyboardOverlay__row">
              {keyRow.keys.map(keyTuple => (
                <li
                  key={keyTuple[0]}
                  class={`key key-${keyTuple[0]}`}
                  onClick={this.emulateKeydown(keyTuple[0])}
                >
                  <span class="keyLabel">{keyTuple[0]}</span>
                  <span class="keyHint">{keyTuple[1]}</span>
                </li>
              ))}
            </ul>
          </li>
        ))}
      </ul>
    );
  }
}

export default KeyboardOverlay;
