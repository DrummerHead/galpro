/** @jsx preact.h */
// @flow

import preact from 'preact';

import type { MediaProps } from '../main.js';

type ImageState = {
  isZooming: boolean,
  yPosition: number,
};
class Image extends preact.Component<MediaProps, ImageState> {
  state = {
    isZooming: false,
    yPosition: 50,
  };

  imageDOM: ?HTMLImageElement = undefined;

  zoomHandle = () => {
    this.setState(prevState => ({
      isZooming: !prevState.isZooming,
      yPosition: prevState.isZooming ? 50 : 0,
    }));
  };

  keyPressHandler = (ev: KeyboardEvent) => {
    if (this.props.isCurrent) {
      switch (ev.key) {
        // Scroll down if zooming
        case 'n':
          if (this.state.isZooming) {
            this.setState(prevState => ({
              yPosition:
                prevState.yPosition + 1 <= 100 ? prevState.yPosition + 1 : 100,
            }));
          }
          break;
        // Zoom toggle
        case 'm':
          this.zoomHandle();
          break;
      }
    }
  };

  mouseMoveHandler = (ev: MouseEvent) => {
    // Mouse position determines "scroll" position on image
    if (this.state.isZooming) {
      this.setState({
        yPosition: ev.clientY * 100 / window.innerHeight,
      });
    }
  };

  componentDidMount() {
    document.addEventListener('keypress', this.keyPressHandler);
    document.addEventListener('mousemove', this.mouseMoveHandler);
    if (this.imageDOM && this.imageDOM.complete) {
      this.props.handleLoaded(this.props.id);
    }
  }

  componentWillUnmount() {
    document.removeEventListener('keypress', this.keyPressHandler);
    document.removeEventListener('mousemove', this.mouseMoveHandler);
  }

  widthStyle = (width: number) =>
    this.props.isCurrent ? {} : { width: `${this.props.width}vw` };

  render() {
    return (
      <div
        className={`picContain image ${
          this.props.isCurrent ? 'currentPic' : 'cachePic'
        }`}
        data-id={this.props.id}
        style={this.widthStyle(this.props.width)}
        onClick={this.props.onClick(this.props.id)}
      >
        <img
          src={this.props.href}
          className={`pic ${this.state.isZooming ? 'imageZoom' : ''}`}
          onLoad={this.props.handleLoaded(this.props.id)}
          onError={this.props.handleError(this.props.id)}
          onDblClick={this.zoomHandle}
          alt={this.props.title}
          style={{ objectPosition: `50% ${this.state.yPosition}%` }}
          ref={elem => (this.imageDOM = elem)}
        />
        {!this.props.hasLoaded &&
          !this.props.hasError && (
            <div className="loading">
              <span className="loadingIcon">&#9203;</span>
            </div>
          )}
        {this.props.hasError && (
          <div className="error">
            <span className="errorMessage">
              The image <code>{this.props.href}</code>, &#8220;{
                this.props.title
              }&#8221;; failed to load.<br /> Press "h" to try and open in a new
              window
            </span>
          </div>
        )}
      </div>
    );
  }
}

export default Image;
