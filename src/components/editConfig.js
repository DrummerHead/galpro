/** @jsx preact.h */
// @flow

import preact from 'preact';
import type { ConfigType } from '../main';
import { addZeros } from '../helpers/generateNumPics';

type EditConfigProps = {
  config: ConfigType,
  current: number,
  handleConfigChange: (
    prevChunk: string,
    current: string,
    postChunk: string
  ) => void,
  handleConfigEsc: () => void,
};

type EditConfigState = {
  prevChunk: string,
  current: string,
  postChunk: string,
};
class EditConfig extends preact.Component<EditConfigProps, EditConfigState> {
  state = {
    prevChunk: this.props.config.prevChunk,
    current: addZeros(String(this.props.current), this.props.config.padZeros),
    postChunk: this.props.config.postChunk,
  };

  componentDidMount() {
    document.addEventListener('keydown', this.keypress);
  }

  componentDidUnmount() {
    document.removeEventListener('keydown', this.keypress);
  }

  keypress = (ev: KeyboardEvent) => {
    switch (ev.key) {
      case 'Escape':
        this.props.handleConfigEsc();
        break;
    }
  };

  handleChange = (field: string) => (
    ev: SyntheticInputEvent<HTMLInputElement>
  ) => {
    this.setState(prevState => ({
      ...prevState,
      [field]: ev.currentTarget.value,
    }));
  };
  handleSubmit = (ev: Event) => {
    ev.preventDefault();
    this.props.handleConfigChange(
      this.state.prevChunk,
      this.state.current,
      this.state.postChunk
    );
  };
  render() {
    return (
      <div class="editConfig">
        <form onSubmit={this.handleSubmit} className="EC__wrap">
          <input
            type="text"
            value={this.state.prevChunk}
            onChange={this.handleChange('prevChunk')}
            style={{ width: `${this.state.prevChunk.length}ch` }}
          />
          <input
            type="text"
            value={this.state.current}
            onChange={this.handleChange('current')}
            style={{ width: `${this.state.current.length}ch` }}
          />
          <input
            type="text"
            value={this.state.postChunk}
            onChange={this.handleChange('postChunk')}
            style={{ width: `${this.state.postChunk.length}ch` }}
          />
          <button type="submit">Go</button>
        </form>
        {!this.props.config.isAnumberedImgHref && (
          <strong className="EC__alert">
            Note: this is not originally a "numbered" gallery,<br />moving
            forward will transform this to a numbered gallery<br />based on the
            parameters provided.<br />Press ESC if you're not sure.
          </strong>
        )}
      </div>
    );
  }
}

export default EditConfig;
