/** @jsx preact.h */
// @flow

import preact from 'preact';

type CounterProps = {
  isLast: boolean,
  current: number,
  picsLength: number,
  changeCurrent: (number, ?boolean) => void,
};

type CounterState = {
  isEditing: boolean,
};

class Counter extends preact.Component<CounterProps, CounterState> {
  state = {
    isEditing: false,
  };

  editCurrent = () => {
    this.setState({ isEditing: true });
  };

  handleChange = (ev: SyntheticInputEvent<HTMLInputElement>) => {
    this.props.changeCurrent(parseInt(ev.target.value) - 1, true);
  };

  handleSubmit = (ev: Event) => {
    ev.preventDefault();
    this.setState({ isEditing: false });
  };

  render() {
    return (
      <div class={`counter ${this.props.isLast ? 'counter--last' : ''}`}>
        <span
          class={`counter__current ${this.state.isEditing ? 'hide' : 'show'}`}
          onClick={this.editCurrent}
        >
          {this.props.current + 1}
        </span>
        <form
          class={`counter__current__edit ${
            this.state.isEditing ? 'show' : 'hide'
          }`}
          onSubmit={this.handleSubmit}
        >
          <input
            class="counter__current__edit__input"
            type="number"
            value={this.props.current + 1}
            onChange={this.handleChange}
          />
        </form>
        <span class="counter__total">{this.props.picsLength + 1}</span>
      </div>
    );
  }
}

export default Counter;
