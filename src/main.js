/** @jsx preact.h */
// @flow

import preact from 'preact';

import Video from './components/video';
import Image from './components/image';
import KeyboardOverlay from './components/keyboardOverlay';
import { KOCVideo, KOCImage, KOCGlobal } from './keyboardOverlayConfig';
import Counter from './components/counter';
import EditConfig from './components/editConfig';
import configParse from './helpers/configParse';
import fullScreen from './helpers/fullScreen';
import generateNumPics from './helpers/generateNumPics';
import PreLoader from './helpers/PreLoader';
import autoGallery from './helpers/autoGallery';
import placeHolderImage from './helpers/placeHolderImage';
import slidePopupHtml from './slidePopup/slidePopup';
import { makeConfig, formatFinder } from './bookmarklet';

import log from './helpers/log';

import type { Pic } from './bookmarklet';

export type PicMeta = Pic & {
  id: number,
  isCurrent: boolean,
  hasError: boolean,
  hasLoaded: boolean,
  isRetrying: boolean,
  retries: number,
  duration: number,
};

export type MediaProps = PicMeta & {
  handleError: number => Event => void,
  handleLoaded: number => Event => void,
  width: number,
  onClick: number => MouseEvent => void,
};

const root = document && document.getElementById('root');

const toggleFullScreen = ({ elem, goFullScreen }) => {
  goFullScreen
    ? fullScreen.requestFullscreen(elem)
    : fullScreen.exitFullscreen();
};

const addMetadata = (imageList: Array<Pic>): Array<PicMeta> =>
  imageList.map((pic: Pic, index): PicMeta => ({
    ...pic,
    id: index,
    isCurrent: false,
    hasError: false,
    hasLoaded: false,
    isRetrying: false,
    retries: 0,
    duration: 0,
  }));

const setPicAsCurrent = (id: number) => (pic: PicMeta): PicMeta => ({
  ...pic,
  isCurrent: pic.id === id,
});

const setInitialPicsState = (
  pics: Array<Pic>,
  config: ConfigType
): { pics: Array<PicMeta>, current: number } =>
  config.isAnumberedImgHref
    ? generateNumPics(pics, config)
    : { pics: addMetadata(pics).map(setPicAsCurrent(0)), current: 0 };

export type ConfigType = {
  isAnumberedImgHref: boolean,
  prevChunk: string,
  postChunk: string,
  hasPadZeros: boolean,
  padZeros: number,
  current: number,
  fullUrl: string,
};
type MainProps = {
  pics: Array<Pic>,
  config: ConfigType,
};
export type MainState = {
  pics: Array<PicMeta>,
  current: number,
  hasPadZeros: boolean,
  isFullScreen: boolean,
  displayGallery: boolean,
  lastDisplayGalleryWasAuto: boolean,
  volume: number,
  nextHasLoaded: boolean,
  nextHasError: boolean,
  autoAdvance: boolean,
  autoAdvanceDelay: number,
  autoAdvanceDelayJustChanged: boolean,
  autoAdvanceTID: TimeoutID,
  autoAdvanceNextLoadingHits: number,
  retryAmount: number,
  isFirst: boolean,
  isLast: boolean,
  showMetadata: boolean,
  cacheSize: number,
  debug: boolean,
  editConfig: boolean,
  slidePopOpen: boolean,
};
class Main extends preact.Component<MainProps, MainState> {
  state = {
    pics: [],
    current: 0,
    hasPadZeros: this.props.config.hasPadZeros,
    isFullScreen: false,
    displayGallery: false,
    lastDisplayGalleryWasAuto: false,
    volume: 0.5,
    nextHasLoaded: false,
    nextHasError: false,
    autoAdvance: false,
    autoAdvanceDelay: 1700,
    autoAdvanceDelayJustChanged: false,
    autoAdvanceTID: setTimeout(() => {}, 16),
    autoAdvanceNextLoadingHits: 0,
    retryAmount: this.props.config.isAnumberedImgHref ? 3 : 7,
    isFirst: true,
    isLast: false,
    showMetadata: false,
    cacheSize: 5,
    debug: false,
    editConfig: false,
    slidePopOpen: false,
  };

  config: ConfigType;

  preLoader: PreLoader;

  slidePop: any;

  findPic = (id: number): PicMeta =>
    this.state.pics.find((pic: PicMeta): boolean => pic.id === id) ||
    placeHolderImage;

  determineCacheArray(): Array<number> {
    let unpure = [];
    for (
      let i = 1;
      i < Math.min(this.state.pics.length, this.state.cacheSize + 1);
      i++
    ) {
      unpure.push(i);
    }
    return unpure;
  }

  increaseCache = () => {
    this.setState(prevState => ({
      cacheSize: prevState.cacheSize + 1,
    }));
  };

  keydown = (ev: KeyboardEvent) => {
    switch (ev.key) {
      case 'h':
        window.open(this.findPic(this.state.current).href);
        break;

      case 'j':
      case 'ArrowRight':
      case 'ArrowDown':
        this.forwards(true);
        break;

      case 'k':
      case 'ArrowUp':
      case 'ArrowLeft':
        this.backwards(true);
        break;

      case 'l':
        this.setState(prevState => ({
          isFullScreen: !prevState.isFullScreen,
        }));
        toggleFullScreen({
          elem: root,
          goFullScreen: this.state.isFullScreen,
        });
        break;

      case 'g':
        const slidePop = window.open(
          'about:blank',
          'popGallery',
          'width=1366,height=768'
        );
        slidePop.document.write(slidePopupHtml);
        slidePop.document.close();
        slidePop.main.setPics(this.state.pics);
        slidePop.main.setCurrent(this.state.current);
        this.slidePop = slidePop;
        this.setState({ slidePopOpen: true });
        break;

      case 'f':
        this.setState(prevState => ({
          showMetadata: !prevState.showMetadata,
        }));
        break;

      case 'd':
        this.setState(prevState => ({
          displayGallery: !prevState.displayGallery,
          lastDisplayGalleryWasAuto: false,
        }));
        break;

      case 's':
        this.setState(prevState => ({
          pics: prevState.pics
            .map((pic, i, arr) => ({ ...pic, id: arr.length - 1 - pic.id }))
            .map(setPicAsCurrent(0)),
          current: 0,
        }));
        break;

      case 'a':
        if (this.config.isAnumberedImgHref) {
          this.setState(prevState => ({
            hasPadZeros: !prevState.hasPadZeros,
            pics: generateNumPics(this.props.pics, {
              ...this.config,
              ...{ hasPadZeros: !prevState.hasPadZeros },
            }).pics.map(setPicAsCurrent(this.state.current)),
          }));
        }
        break;

      case 'r':
        this.jumpTo(0, true);
        this.setState({
          displayGallery: true,
          lastDisplayGalleryWasAuto: true,
        });
        break;

      case 'e':
      case ' ':
        this.autoGalleryToggle();
        break;

      case '-':
        this.setState(prevState => ({
          autoAdvanceDelay: prevState.autoAdvanceDelay + 100,
          autoAdvanceNextLoadingHits: 0,
        }));
        break;

      case '+':
      case '=':
        this.setState(prevState => ({
          autoAdvanceDelay:
            prevState.autoAdvanceDelay > 100
              ? prevState.autoAdvanceDelay - 100
              : 100,
          autoAdvanceNextLoadingHits: 0,
        }));
        break;

      case '§':
      case 'q':
        this.setState(prevState => ({ debug: !prevState.debug }));
        break;

      case '±':
        this.setState({ retryAmount: 0 });
        break;

      case '3':
        this.handleError(this.state.current, true)();
        break;

      case '\\':
        this.unmountKeydown();
        this.setState({ editConfig: true });
        break;
    }
  };

  mountKeydown = () => {
    document.addEventListener('keydown', this.keydown);
  };

  unmountKeydown = () => {
    document.removeEventListener('keydown', this.keydown);
  };

  componentDidMount() {
    this.config = configParse(this.props.config);
    const initialPicsState = setInitialPicsState(this.props.pics, this.config);
    this.setState(initialPicsState);

    this.preLoader = new PreLoader(initialPicsState.pics, 100);

    this.mountKeydown();

    fullScreen.addEventListener('fullscreenchange', () => {
      this.setState({
        isFullScreen: fullScreen.fullscreenElement !== null,
      });
    });

    window.addEventListener('beforeunload', this.itClosed);
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevState.autoAdvanceDelay !== this.state.autoAdvanceDelay) {
      this.setState({ autoAdvanceDelayJustChanged: true });
      setTimeout(
        () => this.setState({ autoAdvanceDelayJustChanged: false }),
        16
      );
    }
    if (this.state.slidePopOpen && prevState.current !== this.state.current) {
      this.slidePop.main.setCurrent(this.state.current);
    }
    if (this.state.slidePopOpen && prevState.pics !== this.state.pics) {
      this.slidePop.main.setPics(this.state.pics);
    }
  }

  itClosed = () => {
    if (this.state.slidePopOpen) {
      this.slidePop.close();
    }
  };

  jumpTo = (id: number, userTriggered?: boolean = false) => {
    const min = 0;
    const max = this.state.pics.length - 1;
    const pic = this.findPic(id);
    const nextPic = this.findPic(id + 1);

    if (id >= max) {
      this.setState(prevState => ({
        pics: prevState.pics.map(setPicAsCurrent(max)),
        current: max,
        ...autoGallery(pic.hasError, prevState),
        autoAdvance: userTriggered ? false : prevState.autoAdvance,
        nextHasLoaded: nextPic.hasLoaded,
        nextHasError: nextPic.hasError,
        isLast: true,
      }));
    } else if (id <= min) {
      this.setState(prevState => ({
        pics: prevState.pics.map(setPicAsCurrent(min)),
        current: min,
        ...autoGallery(pic.hasError, prevState),
        autoAdvance: userTriggered ? false : prevState.autoAdvance,
        nextHasLoaded: nextPic.hasLoaded,
        nextHasError: nextPic.hasError,
        isFirst: true,
      }));
      this.preLoader.setCurrent(min);
    } else {
      this.setState(prevState => ({
        pics: prevState.pics.map(setPicAsCurrent(id)),
        current: id,
        ...autoGallery(pic.hasError, prevState),
        autoAdvance: userTriggered ? false : prevState.autoAdvance,
        nextHasLoaded: nextPic.hasLoaded,
        nextHasError: nextPic.hasError,
        isFirst: false,
        isLast: false,
      }));
      this.preLoader.setCurrent(id);
    }
  };

  backwards = (userTriggered?: boolean = false) => {
    this.jumpTo(this.state.current - 1, userTriggered);
  };

  forwards = (userTriggered?: boolean = false) => {
    this.jumpTo(this.state.current + 1, userTriggered);
  };

  handleError = (id: number, forceRetry?: boolean = false) => (ev?: Event) => {
    const picCache = this.findPic(id);

    if (picCache.retries < this.state.retryAmount || forceRetry) {
      this.setState(prevState => {
        return {
          pics: prevState.pics.map(
            pic =>
              pic.id === id
                ? {
                    ...pic,
                    href:
                      pic.type === 'video'
                        ? 'data:video/webm;base64,GkXfo0AgQoaBAUL3gQFC8oEEQvOBCEKCQAR3ZWJtQoeBAkKFgQIYU4BnQI0VSalmQCgq17FAAw9CQE2AQAZ3aGFtbXlXQUAGd2hhbW15RIlACECPQAAAAAAAFlSua0AxrkAu14EBY8WBAZyBACK1nEADdW5khkAFVl9WUDglhohAA1ZQOIOBAeBABrCBCLqBCB9DtnVAIueBAKNAHIEAAIAwAQCdASoIAAgAAUAmJaQAA3AA/vz0AAA='
                        : 'data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==',
                    retries: pic.retries + 1,
                    hasLoaded: false,
                    isRetrying: true,
                  }
                : pic
          ),
        };
      });
      setTimeout(() => {
        this.setState(prevState => {
          return {
            pics: prevState.pics.map(
              pic =>
                pic.id === id
                  ? {
                      ...pic,
                      href: picCache.href,
                      hasError: false,
                      hasLoaded: false,
                      isRetrying: false,
                    }
                  : pic
            ),
          };
        });
      }, 500);
    } else {
      this.setState(prevState => ({
        pics: prevState.pics.map(
          pic => (pic.id === id ? { ...pic, hasError: true } : pic)
        ),
        nextHasError:
          prevState.current + 1 === id ? true : prevState.nextHasError,
      }));
    }
  };

  handleLoaded = (id: number) => (ev: Event) => {
    const pic = this.findPic(id);
    if (!pic.hasLoaded && !pic.isRetrying) {
      if (pic.isCurrent && pic.id !== 0) {
        this.increaseCache();
      }
      this.setState(prevState => ({
        pics: prevState.pics.map(
          pic =>
            pic.id === id
              ? {
                  ...pic,
                  hasLoaded: true,
                  duration:
                    //$FlowFixMe
                    pic.type === 'video' ? ev.target.duration * 1000 : 0,
                }
              : pic
        ),
        nextHasLoaded:
          prevState.current + 1 === id ? true : prevState.nextHasLoaded,
      }));
    }
  };

  setVolume = ev => {
    this.setState({ volume: ev.currentTarget.value });
  };

  handleClick = (id: number) => (ev: MouseEvent) => {
    const thisPic = this.findPic(id);
    if (!thisPic.isCurrent) {
      this.jumpTo(thisPic.id, true);
    }
  };

  handleConfigEsc = () => {
    this.mountKeydown();
    this.setState({ editConfig: false });
  };

  handleConfigChange = (
    prevChunk: string,
    current: string,
    postChunk: string
  ) => {
    this.mountKeydown();
    this.config = makeConfig(prevChunk, current, postChunk);
    const fullUrl = `${prevChunk}${current}${postChunk}`;
    const sampleImage = {
      href: fullUrl,
      type: formatFinder(fullUrl),
      title: fullUrl,
    };
    this.setState({
      ...generateNumPics([sampleImage], this.config),
      editConfig: false,
    });
  };

  autoGalleryToggle() {
    const advance = () => {
      if (this.state.autoAdvance) {
        if (this.state.nextHasLoaded) {
          this.forwards();
          const currPic = this.findPic(this.state.current);
          const advanceDelay =
            currPic.type === 'video'
              ? Math.ceil(this.state.autoAdvanceDelay / currPic.duration) *
                currPic.duration
              : this.state.autoAdvanceDelay;
          this.setState({
            autoAdvanceTID: setTimeout(advance, advanceDelay),
          });
        } else if (this.state.nextHasError) {
          const skipErrors = () => {
            // TODO: check if I can use this.state.nextHasError
            // again instead of findPiccing
            const nextPic = this.findPic(this.state.current + 1);
            if (nextPic.hasError) {
              this.forwards();
              skipErrors();
            }
          };
          skipErrors();
          advance();
        } else {
          this.setState(prevState => ({
            autoAdvanceTID: setTimeout(advance, 250),
            autoAdvanceNextLoadingHits:
              prevState.autoAdvanceNextLoadingHits >= 16
                ? 0
                : prevState.autoAdvanceNextLoadingHits + 1,
            autoAdvanceDelay:
              prevState.autoAdvanceNextLoadingHits >= 16
                ? prevState.autoAdvanceDelay + 100
                : prevState.autoAdvanceDelay,
          }));
        }
      } else {
        clearTimeout(this.state.autoAdvanceTID);
      }
    };

    if (this.state.autoAdvance) {
      this.setState({ autoAdvance: false, autoAdvanceNextLoadingHits: 0 });
      clearTimeout(this.state.autoAdvanceTID);
    } else {
      this.setState({ autoAdvance: true });
      advance();
    }
  }

  media(id) {
    const thisPic: PicMeta = this.findPic(id % this.state.pics.length);
    switch (thisPic.type) {
      case 'image':
        return (
          <Image
            key={thisPic.id}
            {...thisPic}
            width={100 / this.state.cacheSize}
            handleError={this.handleError}
            handleLoaded={this.handleLoaded}
            onClick={this.handleClick}
          />
        );
      case 'video':
        return (
          <Video
            key={thisPic.id}
            {...thisPic}
            volume={this.state.volume}
            width={100 / this.state.cacheSize}
            handleError={this.handleError}
            handleLoaded={this.handleLoaded}
            onClick={this.handleClick}
          />
        );
      case 'impossibru':
        return "I can't believe you've done this";
    }
  }

  render() {
    const currPic = this.findPic(this.state.current);
    return (
      <main className={this.state.displayGallery ? 'galleryMode' : ''}>
        <div
          className={`pics ${this.state.displayGallery ? 'picsGallery' : ''}`}
        >
          {this.media(this.state.current)}
          {this.determineCacheArray().map((num: number) =>
            this.media(this.state.current + num)
          )}
          {this.media(this.state.current - 1)}
        </div>

        <div
          className={`controls ${this.state.isFirst ? 'isFirst' : ''} ${
            this.state.isLast ? 'isLast' : ''
          }`}
        >
          <div className="backwards" onClick={() => this.backwards(true)} />
          <div className="forwards" onClick={() => this.forwards(true)} />
        </div>

        <Counter
          isLast={this.state.isLast}
          current={this.state.current}
          picsLength={this.state.pics.length - 1}
          changeCurrent={this.jumpTo}
        />

        <div
          className={`${
            this.state.autoAdvanceDelayJustChanged ? '' : 'animateAADT '
          }autoAdvanceDelayTip`}
        >
          {this.state.autoAdvanceDelay}ms
        </div>

        <div
          className={`next-loading ${
            !this.state.nextHasError && !this.state.nextHasLoaded
              ? 'next-loading--isLoading'
              : ''
          }`}
        >
          Loading next...
        </div>

        <KeyboardOverlay
          keys={currPic.type === 'video' ? KOCVideo : KOCImage}
          position={{ right: '0', bottom: '28vh' }}
        />
        <KeyboardOverlay
          keys={KOCGlobal}
          position={{ left: '0', bottom: '28vh' }}
        />

        <input
          className="globalVolume"
          type="range"
          min={0}
          max={1}
          step="any"
          value={this.state.volume}
          onInput={this.setVolume}
        />

        {this.state.debug && (
          <div className="debug">{JSON.stringify(currPic, null, 2)}</div>
        )}

        {this.state.showMetadata && (
          <div className="metaData">
            <ul>
              <li>{currPic.type === 'image' ? '🖼' : '📽'}</li>
              <li>{currPic.title}</li>
              <li>{currPic.href}</li>
            </ul>
          </div>
        )}

        {this.state.editConfig && (
          <EditConfig
            config={this.config}
            current={this.state.current}
            handleConfigChange={this.handleConfigChange}
            handleConfigEsc={this.handleConfigEsc}
          />
        )}
      </main>
    );
  }
}

root &&
  preact.render(
    <Main
      pics={window.pics}
      config={window.config}
      ref={main => (window.main = main)}
    />,
    root
  );
