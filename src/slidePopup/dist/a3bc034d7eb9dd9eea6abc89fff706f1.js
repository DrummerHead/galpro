require = (function(r, e, n) {
  function t(n, o) {
    function i(r) {
      return t(i.resolve(r));
    }
    function f(e) {
      return r[n][1][e] || e;
    }
    if (!e[n]) {
      if (!r[n]) {
        var c = 'function' == typeof require && require;
        if (!o && c) return c(n, !0);
        if (u) return u(n, !0);
        var l = new Error("Cannot find module '" + n + "'");
        throw ((l.code = 'MODULE_NOT_FOUND'), l);
      }
      i.resolve = f;
      var a = (e[n] = new t.Module());
      r[n][0].call(a.exports, i, a, a.exports);
    }
    return e[n].exports;
  }
  function o() {
    (this.bundle = t), (this.exports = {});
  }
  var u = 'function' == typeof require && require;
  (t.Module = o), (t.modules = r), (t.cache = e), (t.parent = u);
  for (var i = 0; i < n.length; i++) t(n[i]);
  return t;
})(
  {
    9: [
      function(require, module, exports) {
        'use strict';
        function e() {}
        Object.defineProperty(exports, '__esModule', { value: !0 });
        var t = {},
          n = [],
          o = [];
        function r(r, i) {
          var l,
            a,
            p,
            s,
            c = o;
          for (s = arguments.length; s-- > 2; ) n.push(arguments[s]);
          for (
            i &&
            null != i.children &&
            (n.length || n.push(i.children), delete i.children);
            n.length;

          )
            if ((a = n.pop()) && void 0 !== a.pop)
              for (s = a.length; s--; ) n.push(a[s]);
            else
              'boolean' == typeof a && (a = null),
                (p = 'function' != typeof r) &&
                  (null == a
                    ? (a = '')
                    : 'number' == typeof a
                      ? (a = String(a))
                      : 'string' != typeof a && (p = !1)),
                p && l
                  ? (c[c.length - 1] += a)
                  : c === o ? (c = [a]) : c.push(a),
                (l = p);
          var u = new e();
          return (
            (u.nodeName = r),
            (u.children = c),
            (u.attributes = null == i ? void 0 : i),
            (u.key = null == i ? void 0 : i.key),
            void 0 !== t.vnode && t.vnode(u),
            u
          );
        }
        function i(e, t) {
          for (var n in t) e[n] = t[n];
          return e;
        }
        var l =
          'function' == typeof Promise
            ? Promise.resolve().then.bind(Promise.resolve())
            : setTimeout;
        function a(e, t) {
          return r(
            e.nodeName,
            i(i({}, e.attributes), t),
            arguments.length > 2 ? [].slice.call(arguments, 2) : e.children
          );
        }
        var p = /acit|ex(?:s|g|n|p|$)|rph|ows|mnc|ntw|ine[ch]|zoo|^ord/i,
          s = [];
        function c(e) {
          !e._dirty &&
            (e._dirty = !0) &&
            1 == s.push(e) &&
            (t.debounceRendering || l)(u);
        }
        function u() {
          var e,
            t = s;
          for (s = []; (e = t.pop()); ) e._dirty && A(e);
        }
        function f(e, t, n) {
          return 'string' == typeof t || 'number' == typeof t
            ? void 0 !== e.splitText
            : 'string' == typeof t.nodeName
              ? !e._componentConstructor && d(e, t.nodeName)
              : n || e._componentConstructor === t.nodeName;
        }
        function d(e, t) {
          return (
            e.normalizedNodeName === t ||
            e.nodeName.toLowerCase() === t.toLowerCase()
          );
        }
        function _(e) {
          var t = i({}, e.attributes);
          t.children = e.children;
          var n = e.nodeName.defaultProps;
          if (void 0 !== n) for (var o in n) void 0 === t[o] && (t[o] = n[o]);
          return t;
        }
        function v(e, t) {
          var n = t
            ? document.createElementNS('http://www.w3.org/2000/svg', e)
            : document.createElement(e);
          return (n.normalizedNodeName = e), n;
        }
        function m(e) {
          var t = e.parentNode;
          t && t.removeChild(e);
        }
        function h(e, t, n, o, r) {
          if (('className' === t && (t = 'class'), 'key' === t));
          else if ('ref' === t) n && n(null), o && o(e);
          else if ('class' !== t || r)
            if ('style' === t) {
              if (
                ((o && 'string' != typeof o && 'string' != typeof n) ||
                  (e.style.cssText = o || ''),
                o && 'object' == typeof o)
              ) {
                if ('string' != typeof n)
                  for (var i in n) i in o || (e.style[i] = '');
                for (var i in o)
                  e.style[i] =
                    'number' == typeof o[i] && !1 === p.test(i)
                      ? o[i] + 'px'
                      : o[i];
              }
            } else if ('dangerouslySetInnerHTML' === t)
              o && (e.innerHTML = o.__html || '');
            else if ('o' == t[0] && 'n' == t[1]) {
              var l = t !== (t = t.replace(/Capture$/, ''));
              (t = t.toLowerCase().substring(2)),
                o
                  ? n || e.addEventListener(t, y, l)
                  : e.removeEventListener(t, y, l),
                ((e._listeners || (e._listeners = {}))[t] = o);
            } else if ('list' !== t && 'type' !== t && !r && t in e)
              b(e, t, null == o ? '' : o),
                (null != o && !1 !== o) || e.removeAttribute(t);
            else {
              var a = r && t !== (t = t.replace(/^xlink:?/, ''));
              null == o || !1 === o
                ? a
                  ? e.removeAttributeNS(
                      'http://www.w3.org/1999/xlink',
                      t.toLowerCase()
                    )
                  : e.removeAttribute(t)
                : 'function' != typeof o &&
                  (a
                    ? e.setAttributeNS(
                        'http://www.w3.org/1999/xlink',
                        t.toLowerCase(),
                        o
                      )
                    : e.setAttribute(t, o));
            }
          else e.className = o || '';
        }
        function b(e, t, n) {
          try {
            e[t] = n;
          } catch (e) {}
        }
        function y(e) {
          return this._listeners[e.type]((t.event && t.event(e)) || e);
        }
        var x = [],
          C = 0,
          g = !1,
          N = !1;
        function k() {
          for (var e; (e = x.pop()); )
            t.afterMount && t.afterMount(e),
              e.componentDidMount && e.componentDidMount();
        }
        function w(e, t, n, o, r, i) {
          C++ ||
            ((g = null != r && void 0 !== r.ownerSVGElement),
            (N = null != e && !('__preactattr_' in e)));
          var l = S(e, t, n, o, i);
          return (
            r && l.parentNode !== r && r.appendChild(l),
            --C || ((N = !1), i || k()),
            l
          );
        }
        function S(e, t, n, o, r) {
          var i = e,
            l = g;
          if (
            ((null != t && 'boolean' != typeof t) || (t = ''),
            'string' == typeof t || 'number' == typeof t)
          )
            return (
              e &&
              void 0 !== e.splitText &&
              e.parentNode &&
              (!e._component || r)
                ? e.nodeValue != t && (e.nodeValue = t)
                : ((i = document.createTextNode(t)),
                  e &&
                    (e.parentNode && e.parentNode.replaceChild(i, e),
                    L(e, !0))),
              (i.__preactattr_ = !0),
              i
            );
          var a = t.nodeName;
          if ('function' == typeof a) return D(e, t, n, o);
          if (
            ((g = 'svg' === a || ('foreignObject' !== a && g)),
            (a = String(a)),
            (!e || !d(e, a)) && ((i = v(a, g)), e))
          ) {
            for (; e.firstChild; ) i.appendChild(e.firstChild);
            e.parentNode && e.parentNode.replaceChild(i, e), L(e, !0);
          }
          var p = i.firstChild,
            s = i.__preactattr_,
            c = t.children;
          if (null == s) {
            s = i.__preactattr_ = {};
            for (var u = i.attributes, f = u.length; f--; )
              s[u[f].name] = u[f].value;
          }
          return (
            !N &&
            c &&
            1 === c.length &&
            'string' == typeof c[0] &&
            null != p &&
            void 0 !== p.splitText &&
            null == p.nextSibling
              ? p.nodeValue != c[0] && (p.nodeValue = c[0])
              : ((c && c.length) || null != p) &&
                U(i, c, n, o, N || null != s.dangerouslySetInnerHTML),
            P(i, t.attributes, s),
            (g = l),
            i
          );
        }
        function U(e, t, n, o, r) {
          var i,
            l,
            a,
            p,
            s,
            c = e.childNodes,
            u = [],
            d = {},
            _ = 0,
            v = 0,
            h = c.length,
            b = 0,
            y = t ? t.length : 0;
          if (0 !== h)
            for (var x = 0; x < h; x++) {
              var C = c[x],
                g = C.__preactattr_;
              null !=
              (N = y && g ? (C._component ? C._component.__key : g.key) : null)
                ? (_++, (d[N] = C))
                : (g ||
                    (void 0 !== C.splitText ? !r || C.nodeValue.trim() : r)) &&
                  (u[b++] = C);
            }
          if (0 !== y)
            for (x = 0; x < y; x++) {
              var N;
              if (((s = null), null != (N = (p = t[x]).key)))
                _ && void 0 !== d[N] && ((s = d[N]), (d[N] = void 0), _--);
              else if (!s && v < b)
                for (i = v; i < b; i++)
                  if (void 0 !== u[i] && f((l = u[i]), p, r)) {
                    (s = l),
                      (u[i] = void 0),
                      i === b - 1 && b--,
                      i === v && v++;
                    break;
                  }
              (s = S(s, p, n, o)),
                (a = c[x]),
                s &&
                  s !== e &&
                  s !== a &&
                  (null == a
                    ? e.appendChild(s)
                    : s === a.nextSibling ? m(a) : e.insertBefore(s, a));
            }
          if (_) for (var x in d) void 0 !== d[x] && L(d[x], !1);
          for (; v <= b; ) void 0 !== (s = u[b--]) && L(s, !1);
        }
        function L(e, t) {
          var n = e._component;
          n
            ? H(n)
            : (null != e.__preactattr_ &&
                e.__preactattr_.ref &&
                e.__preactattr_.ref(null),
              (!1 !== t && null != e.__preactattr_) || m(e),
              M(e));
        }
        function M(e) {
          for (e = e.lastChild; e; ) {
            var t = e.previousSibling;
            L(e, !0), (e = t);
          }
        }
        function P(e, t, n) {
          var o;
          for (o in n)
            (t && null != t[o]) ||
              null == n[o] ||
              h(e, o, n[o], (n[o] = void 0), g);
          for (o in t)
            'children' === o ||
              'innerHTML' === o ||
              (o in n &&
                t[o] === ('value' === o || 'checked' === o ? e[o] : n[o])) ||
              h(e, o, n[o], (n[o] = t[o]), g);
        }
        var T = {};
        function B(e) {
          var t = e.constructor.name;
          (T[t] || (T[t] = [])).push(e);
        }
        function E(e, t, n) {
          var o,
            r = T[e.name];
          if (
            (e.prototype && e.prototype.render
              ? ((o = new e(t, n)), j.call(o, t, n))
              : (((o = new j(t, n)).constructor = e), (o.render = W)),
            r)
          )
            for (var i = r.length; i--; )
              if (r[i].constructor === e) {
                (o.nextBase = r[i].nextBase), r.splice(i, 1);
                break;
              }
          return o;
        }
        function W(e, t, n) {
          return this.constructor(e, n);
        }
        function V(e, n, o, r, i) {
          e._disable ||
            ((e._disable = !0),
            (e.__ref = n.ref) && delete n.ref,
            (e.__key = n.key) && delete n.key,
            !e.base || i
              ? e.componentWillMount && e.componentWillMount()
              : e.componentWillReceiveProps &&
                e.componentWillReceiveProps(n, r),
            r &&
              r !== e.context &&
              (e.prevContext || (e.prevContext = e.context), (e.context = r)),
            e.prevProps || (e.prevProps = e.props),
            (e.props = n),
            (e._disable = !1),
            0 !== o &&
              (1 !== o && !1 === t.syncComponentUpdates && e.base
                ? c(e)
                : A(e, 1, i)),
            e.__ref && e.__ref(e));
        }
        function A(e, n, o, r) {
          if (!e._disable) {
            var l,
              a,
              p,
              s = e.props,
              c = e.state,
              u = e.context,
              f = e.prevProps || s,
              d = e.prevState || c,
              v = e.prevContext || u,
              m = e.base,
              h = e.nextBase,
              b = m || h,
              y = e._component,
              g = !1;
            if (
              (m &&
                ((e.props = f),
                (e.state = d),
                (e.context = v),
                2 !== n &&
                e.shouldComponentUpdate &&
                !1 === e.shouldComponentUpdate(s, c, u)
                  ? (g = !0)
                  : e.componentWillUpdate && e.componentWillUpdate(s, c, u),
                (e.props = s),
                (e.state = c),
                (e.context = u)),
              (e.prevProps = e.prevState = e.prevContext = e.nextBase = null),
              (e._dirty = !1),
              !g)
            ) {
              (l = e.render(s, c, u)),
                e.getChildContext && (u = i(i({}, u), e.getChildContext()));
              var N,
                S,
                U = l && l.nodeName;
              if ('function' == typeof U) {
                var M = _(l);
                (a = y) && a.constructor === U && M.key == a.__key
                  ? V(a, M, 1, u, !1)
                  : ((N = a),
                    (e._component = a = E(U, M, u)),
                    (a.nextBase = a.nextBase || h),
                    (a._parentComponent = e),
                    V(a, M, 0, u, !1),
                    A(a, 1, o, !0)),
                  (S = a.base);
              } else
                (p = b),
                  (N = y) && (p = e._component = null),
                  (b || 1 === n) &&
                    (p && (p._component = null),
                    (S = w(p, l, u, o || !m, b && b.parentNode, !0)));
              if (b && S !== b && a !== y) {
                var P = b.parentNode;
                P &&
                  S !== P &&
                  (P.replaceChild(S, b),
                  N || ((b._component = null), L(b, !1)));
              }
              if ((N && H(N), (e.base = S), S && !r)) {
                for (var T = e, B = e; (B = B._parentComponent); )
                  (T = B).base = S;
                (S._component = T), (S._componentConstructor = T.constructor);
              }
            }
            if (
              (!m || o
                ? x.unshift(e)
                : g ||
                  (e.componentDidUpdate && e.componentDidUpdate(f, d, v),
                  t.afterUpdate && t.afterUpdate(e)),
              null != e._renderCallbacks)
            )
              for (; e._renderCallbacks.length; )
                e._renderCallbacks.pop().call(e);
            C || r || k();
          }
        }
        function D(e, t, n, o) {
          for (
            var r = e && e._component,
              i = r,
              l = e,
              a = r && e._componentConstructor === t.nodeName,
              p = a,
              s = _(t);
            r && !p && (r = r._parentComponent);

          )
            p = r.constructor === t.nodeName;
          return (
            r && p && (!o || r._component)
              ? (V(r, s, 3, n, o), (e = r.base))
              : (i && !a && (H(i), (e = l = null)),
                (r = E(t.nodeName, s, n)),
                e && !r.nextBase && ((r.nextBase = e), (l = null)),
                V(r, s, 1, n, o),
                (e = r.base),
                l && e !== l && ((l._component = null), L(l, !1))),
            e
          );
        }
        function H(e) {
          t.beforeUnmount && t.beforeUnmount(e);
          var n = e.base;
          (e._disable = !0),
            e.componentWillUnmount && e.componentWillUnmount(),
            (e.base = null);
          var o = e._component;
          o
            ? H(o)
            : n &&
              (n.__preactattr_ &&
                n.__preactattr_.ref &&
                n.__preactattr_.ref(null),
              (e.nextBase = n),
              m(n),
              B(e),
              M(n)),
            e.__ref && e.__ref(null);
        }
        function j(e, t) {
          (this._dirty = !0),
            (this.context = t),
            (this.props = e),
            (this.state = this.state || {});
        }
        function z(e, t, n) {
          return w(n, e, {}, !1, t, !1);
        }
        i(j.prototype, {
          setState: function(e, t) {
            var n = this.state;
            this.prevState || (this.prevState = i({}, n)),
              i(n, 'function' == typeof e ? e(n, this.props) : e),
              t &&
                (this._renderCallbacks = this._renderCallbacks || []).push(t),
              c(this);
          },
          forceUpdate: function(e) {
            e && (this._renderCallbacks = this._renderCallbacks || []).push(e),
              A(this, 2);
          },
          render: function() {},
        });
        var R = {
          h: r,
          createElement: r,
          cloneElement: a,
          Component: j,
          render: z,
          rerender: u,
          options: t,
        };
        (exports.default = R),
          (exports.h = r),
          (exports.createElement = r),
          (exports.cloneElement = a),
          (exports.Component = j),
          (exports.render = z),
          (exports.rerender = u),
          (exports.options = t);
      },
      {},
    ],
    5: [
      function(require, module, exports) {
        'use strict';
        Object.defineProperty(exports, '__esModule', { value: !0 });
        var e = (function() {
            function e(e, t) {
              for (var a = 0; a < t.length; a++) {
                var i = t[a];
                (i.enumerable = i.enumerable || !1),
                  (i.configurable = !0),
                  'value' in i && (i.writable = !0),
                  Object.defineProperty(e, i.key, i);
              }
            }
            return function(t, a, i) {
              return a && e(t.prototype, a), i && e(t, i), t;
            };
          })(),
          t = require('preact'),
          a = i(t);
        function i(e) {
          return e && e.__esModule ? e : { default: e };
        }
        function r(e, t) {
          if (!(e instanceof t))
            throw new TypeError('Cannot call a class as a function');
        }
        function n(e, t) {
          if (!e)
            throw new ReferenceError(
              "this hasn't been initialised - super() hasn't been called"
            );
          return !t || ('object' != typeof t && 'function' != typeof t) ? e : t;
        }
        function s(e, t) {
          if ('function' != typeof t && null !== t)
            throw new TypeError(
              'Super expression must either be null or a function, not ' +
                typeof t
            );
          (e.prototype = Object.create(t && t.prototype, {
            constructor: {
              value: e,
              enumerable: !1,
              writable: !0,
              configurable: !0,
            },
          })),
            t &&
              (Object.setPrototypeOf
                ? Object.setPrototypeOf(e, t)
                : (e.__proto__ = t));
        }
        var o = (function(t) {
          function i() {
            var e, t, a;
            r(this, i);
            for (var s = arguments.length, o = Array(s), l = 0; l < s; l++)
              o[l] = arguments[l];
            return (
              (t = a = n(
                this,
                (e = i.__proto__ || Object.getPrototypeOf(i)).call.apply(
                  e,
                  [this].concat(o)
                )
              )),
              (a.timeDelta = 5),
              (a.playBackSteps = [0.1, 0.25, 0.5, 0.75, 1, 1.5, 2, 4, 6, 10]),
              (a.playBackStepRegular = 4),
              (a.state = {
                isPlaying: !1,
                playbackStep: a.playBackStepRegular,
              }),
              (a.keyPressHandler = function(e) {
                if (a.props.isCurrent)
                  switch (e.key) {
                    case 'u':
                      a.toggle();
                      break;
                    case 'i':
                      a.seek(-a.timeDelta);
                      break;
                    case 'o':
                      a.seek(a.timeDelta);
                      break;
                    case 'n':
                      a.handlePlaybackRate({ slower: !0 });
                      break;
                    case 'm':
                      a.handlePlaybackRate({ faster: !0 });
                      break;
                    case ',':
                      a.handlePlaybackRate({ reset: !0 });
                  }
              }),
              (a.widthStyle = function(e) {
                return a.props.isCurrent ? {} : { width: a.props.width + 'vw' };
              }),
              n(a, t)
            );
          }
          return (
            s(i, a.default.Component),
            e(i, [
              {
                key: 'managePlay',
                value: function() {
                  this.videoDom &&
                    (this.state.isPlaying
                      ? this.videoDom.play()
                      : this.videoDom.pause());
                },
              },
              {
                key: 'seek',
                value: function(e) {
                  this.videoDom && (this.videoDom.currentTime += e);
                },
              },
              {
                key: 'toggle',
                value: function() {
                  this.setState(function(e) {
                    return { isPlaying: !e.isPlaying };
                  }),
                    this.managePlay();
                },
              },
              {
                key: 'handlePlaybackRate',
                value: function(e) {
                  var t = e.faster,
                    a = void 0 !== t && t,
                    i = e.slower,
                    r = void 0 !== i && i,
                    n = e.reset,
                    s = void 0 !== n && n;
                  a && this.state.playbackStep < this.playBackSteps.length - 1
                    ? this.setState(function(e) {
                        return { playbackStep: e.playbackStep + 1 };
                      })
                    : r && this.state.playbackStep > 0
                      ? this.setState(function(e) {
                          return { playbackStep: e.playbackStep - 1 };
                        })
                      : s &&
                        this.setState({
                          playbackStep: this.playBackStepRegular,
                        }),
                    this.videoDom &&
                      (this.videoDom.playbackRate = this.playBackSteps[
                        this.state.playbackStep
                      ]);
                },
              },
              {
                key: 'componentDidMount',
                value: function() {
                  this.setState({ isPlaying: this.props.isCurrent }),
                    this.managePlay(),
                    document.addEventListener('keypress', this.keyPressHandler);
                },
              },
              {
                key: 'componentWillUnmount',
                value: function() {
                  (this.videoDom = void 0),
                    document.removeEventListener(
                      'keypress',
                      this.keyPressHandler
                    );
                },
              },
              {
                key: 'componentWillReceiveProps',
                value: function(e) {
                  this.props.isCurrent !== e.isCurrent &&
                    (this.setState({ isPlaying: e.isCurrent }),
                    this.managePlay());
                },
              },
              {
                key: 'render',
                value: function() {
                  var e = this;
                  return a.default.h(
                    'div',
                    {
                      className:
                        'picContain ' +
                        (this.props.isCurrent ? 'currentPic' : 'cachePic'),
                      'data-id': this.props.id,
                      style: this.widthStyle(this.props.width),
                    },
                    a.default.h('video', {
                      src: this.props.href,
                      className: 'pic',
                      controls: this.props.isCurrent,
                      loop: !0,
                      preload: 'auto',
                      volume: this.props.volume,
                      onCanPlayThrough: this.props.handleLoaded(this.props.id),
                      onError: this.props.handleError(this.props.id),
                      ref: function(t) {
                        return (e.videoDom = t);
                      },
                    }),
                    a.default.h('div', {
                      className: 'videoGlass',
                      onClick: this.props.onClick(this.props.id),
                    })
                  );
                },
              },
            ]),
            i
          );
        })();
        exports.default = o;
      },
      { preact: 9 },
    ],
    6: [
      function(require, module, exports) {
        'use strict';
        Object.defineProperty(exports, '__esModule', { value: !0 });
        var e = (function() {
            function e(e, t) {
              for (var o = 0; o < t.length; o++) {
                var n = t[o];
                (n.enumerable = n.enumerable || !1),
                  (n.configurable = !0),
                  'value' in n && (n.writable = !0),
                  Object.defineProperty(e, n.key, n);
              }
            }
            return function(t, o, n) {
              return o && e(t.prototype, o), n && e(t, n), t;
            };
          })(),
          t = require('preact'),
          o = n(t);
        function n(e) {
          return e && e.__esModule ? e : { default: e };
        }
        function i(e, t) {
          if (!(e instanceof t))
            throw new TypeError('Cannot call a class as a function');
        }
        function r(e, t) {
          if (!e)
            throw new ReferenceError(
              "this hasn't been initialised - super() hasn't been called"
            );
          return !t || ('object' != typeof t && 'function' != typeof t) ? e : t;
        }
        function s(e, t) {
          if ('function' != typeof t && null !== t)
            throw new TypeError(
              'Super expression must either be null or a function, not ' +
                typeof t
            );
          (e.prototype = Object.create(t && t.prototype, {
            constructor: {
              value: e,
              enumerable: !1,
              writable: !0,
              configurable: !0,
            },
          })),
            t &&
              (Object.setPrototypeOf
                ? Object.setPrototypeOf(e, t)
                : (e.__proto__ = t));
        }
        var a = (function(t) {
          function n() {
            var e, t, o;
            i(this, n);
            for (var s = arguments.length, a = Array(s), u = 0; u < s; u++)
              a[u] = arguments[u];
            return (
              (t = o = r(
                this,
                (e = n.__proto__ || Object.getPrototypeOf(n)).call.apply(
                  e,
                  [this].concat(a)
                )
              )),
              (o.state = { isZooming: !1, yPosition: 50 }),
              (o.imageDOM = void 0),
              (o.zoomHandle = function() {
                o.setState(function(e) {
                  return {
                    isZooming: !e.isZooming,
                    yPosition: e.isZooming ? 50 : 0,
                  };
                });
              }),
              (o.keyPressHandler = function(e) {
                if (o.props.isCurrent)
                  switch (e.key) {
                    case 'n':
                      o.state.isZooming &&
                        o.setState(function(e) {
                          return {
                            yPosition:
                              e.yPosition + 1 <= 100 ? e.yPosition + 1 : 100,
                          };
                        });
                      break;
                    case 'm':
                      o.zoomHandle();
                  }
              }),
              (o.mouseMoveHandler = function(e) {
                o.state.isZooming &&
                  o.setState({
                    yPosition: 100 * e.clientY / window.innerHeight,
                  });
              }),
              (o.widthStyle = function(e) {
                return o.props.isCurrent ? {} : { width: o.props.width + 'vw' };
              }),
              r(o, t)
            );
          }
          return (
            s(n, o.default.Component),
            e(n, [
              {
                key: 'componentDidMount',
                value: function() {
                  document.addEventListener('keypress', this.keyPressHandler),
                    document.addEventListener(
                      'mousemove',
                      this.mouseMoveHandler
                    ),
                    this.imageDOM &&
                      this.imageDOM.complete &&
                      this.props.handleLoaded(this.props.id);
                },
              },
              {
                key: 'componentWillUnmount',
                value: function() {
                  document.removeEventListener(
                    'keypress',
                    this.keyPressHandler
                  ),
                    document.removeEventListener(
                      'mousemove',
                      this.mouseMoveHandler
                    );
                },
              },
              {
                key: 'render',
                value: function() {
                  var e = this;
                  return o.default.h(
                    'div',
                    {
                      className:
                        'picContain ' +
                        (this.props.isCurrent ? 'currentPic' : 'cachePic'),
                      'data-id': this.props.id,
                      style: this.widthStyle(this.props.width),
                      onClick: this.props.onClick(this.props.id),
                    },
                    o.default.h('img', {
                      src: this.props.href,
                      className:
                        'pic ' + (this.state.isZooming ? 'imageZoom' : ''),
                      onLoad: this.props.handleLoaded(this.props.id),
                      onError: this.props.handleError(this.props.id),
                      onDblClick: this.zoomHandle,
                      alt: this.props.title,
                      style: {
                        objectPosition: '50% ' + this.state.yPosition + '%',
                      },
                      ref: function(t) {
                        return (e.imageDOM = t);
                      },
                    }),
                    !this.props.hasLoaded &&
                      !this.props.hasError &&
                      o.default.h(
                        'div',
                        { className: 'loading' },
                        o.default.h('span', { className: 'loadingIcon' }, '⏳')
                      ),
                    this.props.hasError &&
                      o.default.h(
                        'div',
                        { className: 'error' },
                        o.default.h(
                          'span',
                          { className: 'errorMessage' },
                          'The image ',
                          o.default.h('code', null, this.props.href),
                          ', “',
                          this.props.title,
                          '”; failed to load.',
                          o.default.h('br', null),
                          ' Press "h" to try and open in a new window'
                        )
                      )
                  );
                },
              },
            ]),
            n
          );
        })();
        exports.default = a;
      },
      { preact: 9 },
    ],
    8: [
      function(require, module, exports) {
        'use strict';
        Object.defineProperty(exports, '__esModule', { value: !0 });
        var e = {
            fullscreenElement: 0,
            requestFullscreen: 1,
            exitFullscreen: 2,
            fullscreenchange: 3,
          },
          n = [
            'webkitFullscreenElement',
            'webkitRequestFullscreen',
            'webkitExitFullscreen',
            'webkitfullscreenchange',
          ],
          l = [
            'mozFullScreenElement',
            'mozRequestFullScreen',
            'mozCancelFullScreen',
            'mozfullscreenchange',
          ],
          t = [
            'msFullscreenElement',
            'msRequestFullscreen',
            'msExitFullscreen',
            'MSFullscreenChange',
          ],
          u =
            ('fullscreenEnabled' in document && Object.keys(e)) ||
            (n[0] in document && n) ||
            (l[0] in document && l) ||
            (t[0] in document && t) ||
            [];
        exports.default = {
          requestFullscreen: function(n) {
            return n[u[e.requestFullscreen]]();
          },
          requestFullscreenFunction: function(n) {
            return n[u[e.requestFullscreen]];
          },
          get exitFullscreen() {
            return document[u[e.exitFullscreen]].bind(document);
          },
          addEventListener: function(n, l, t) {
            return document.addEventListener(u[e[n]], l, t);
          },
          removeEventListener: function(n, l, t) {
            return document.removeEventListener(u[e[n]], l, t);
          },
          get fullscreenElement() {
            return document[u[e.fullscreenElement]];
          },
        };
      },
      {},
    ],
    7: [
      function(require, module, exports) {
        'use strict';
        Object.defineProperty(exports, '__esModule', { value: !0 });
        var A = {
          id: 404,
          href:
            'data:image/gif;base64,R0lGODlhAQABAIAAAMLCwgAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw==',
          type: 'image',
          title: 'Placeholder image',
          isCurrent: !1,
          hasError: !1,
          hasLoaded: !0,
          isRetrying: !1,
          retries: 0,
        };
        exports.default = A;
      },
      {},
    ],
    3: [
      function(require, module, exports) {
        'use strict';
        var e =
            Object.assign ||
            function(e) {
              for (var t = 1; t < arguments.length; t++) {
                var n = arguments[t];
                for (var r in n)
                  Object.prototype.hasOwnProperty.call(n, r) && (e[r] = n[r]);
              }
              return e;
            },
          t = (function() {
            function e(e, t) {
              for (var n = 0; n < t.length; n++) {
                var r = t[n];
                (r.enumerable = r.enumerable || !1),
                  (r.configurable = !0),
                  'value' in r && (r.writable = !0),
                  Object.defineProperty(e, r.key, r);
              }
            }
            return function(t, n, r) {
              return n && e(t.prototype, n), r && e(t, r), t;
            };
          })(),
          n = require('preact'),
          r = f(n),
          a = require('../components/video'),
          o = f(a),
          i = require('../components/image'),
          s = f(i),
          u = require('../helpers/fullScreen'),
          c = f(u),
          l = require('../helpers/placeHolderImage'),
          d = f(l);
        function f(e) {
          return e && e.__esModule ? e : { default: e };
        }
        function h(e, t) {
          if (!(e instanceof t))
            throw new TypeError('Cannot call a class as a function');
        }
        function m(e, t) {
          if (!e)
            throw new ReferenceError(
              "this hasn't been initialised - super() hasn't been called"
            );
          return !t || ('object' != typeof t && 'function' != typeof t) ? e : t;
        }
        function p(e, t) {
          if ('function' != typeof t && null !== t)
            throw new TypeError(
              'Super expression must either be null or a function, not ' +
                typeof t
            );
          (e.prototype = Object.create(t && t.prototype, {
            constructor: {
              value: e,
              enumerable: !1,
              writable: !0,
              configurable: !0,
            },
          })),
            t &&
              (Object.setPrototypeOf
                ? Object.setPrototypeOf(e, t)
                : (e.__proto__ = t));
        }
        var w = document && document.getElementById('root'),
          v = function(e) {
            var t = e.elem;
            e.goFullScreen
              ? c.default.requestFullscreen(t)
              : c.default.exitFullscreen();
          },
          y = (function(n) {
            function a() {
              var e, t, n;
              h(this, a);
              for (var r = arguments.length, o = Array(r), i = 0; i < r; i++)
                o[i] = arguments[i];
              return (
                (t = n = m(
                  this,
                  (e = a.__proto__ || Object.getPrototypeOf(a)).call.apply(
                    e,
                    [this].concat(o)
                  )
                )),
                (n.state = {
                  pics: [],
                  current: 0,
                  columns: 5,
                  rows: 3,
                  isFullScreen: !1,
                }),
                (n.findPic = function(e) {
                  return (
                    n.state.pics.find(function(t) {
                      return t.id === e;
                    }) || d.default
                  );
                }),
                (n.keypress = function(e) {
                  switch (e.key) {
                    case 'j':
                    case 'ArrowRight':
                    case 'ArrowDown':
                      window.opener.main.forwards(!0);
                      break;
                    case 'k':
                    case 'ArrowUp':
                    case 'ArrowLeft':
                      window.opener.main.backwards(!0);
                      break;
                    case 'l':
                      n.setState(function(e) {
                        return { isFullScreen: !e.isFullScreen };
                      }),
                        v({ elem: w, goFullScreen: n.state.isFullScreen });
                      break;
                    case 'f':
                      window.opener.main.setState(function(e) {
                        return { showMetadata: !e.showMetadata };
                      });
                      break;
                    case 'e':
                    case ' ':
                      window.opener.main.autoGalleryToggle();
                      break;
                    case '-':
                      window.opener.main.setState(function(e) {
                        return {
                          autoAdvanceDelay: e.autoAdvanceDelay + 100,
                          autoAdvanceNextLoadingHits: 0,
                        };
                      });
                      break;
                    case '+':
                    case '=':
                      window.opener.main.setState(function(e) {
                        return {
                          autoAdvanceDelay:
                            e.autoAdvanceDelay > 100
                              ? e.autoAdvanceDelay - 100
                              : 100,
                          autoAdvanceNextLoadingHits: 0,
                        };
                      });
                      break;
                    case '§':
                    case 'q':
                      window.opener.main.setState(function(e) {
                        return { debug: !e.debug };
                      });
                      break;
                    case '±':
                      window.opener.main.setState({ retryAmount: 0 });
                      break;
                    case '3':
                      window.opener.main.handleError(
                        window.opener.main.state.current,
                        !0
                      )();
                  }
                }),
                (n.mountKeypress = function() {
                  document.addEventListener('keypress', n.keypress);
                }),
                (n.unmountKeypress = function() {
                  document.removeEventListener('keypress', n.keypress);
                }),
                (n.setPics = function(e) {
                  n.setState({ pics: e });
                }),
                (n.setCurrent = function(e) {
                  n.setState({ current: e });
                }),
                (n.setColumns = function(e) {
                  return function(t) {
                    n.setState(function(t) {
                      return { columns: e ? t.columns + 1 : t.columns - 1 };
                    });
                  };
                }),
                (n.setRows = function(e) {
                  return function(t) {
                    n.setState(function(t) {
                      return { rows: e ? t.rows + 1 : t.rows - 1 };
                    });
                  };
                }),
                (n.handleClick = function(e) {
                  return function(t) {
                    var r = n.findPic(e);
                    window.opener.main.jumpTo(r.id, !0);
                  };
                }),
                m(n, t)
              );
            }
            return (
              p(a, r.default.Component),
              t(a, [
                {
                  key: 'determineSlidesArray',
                  value: function() {
                    for (
                      var e = [], t = 1;
                      t <
                      Math.min(
                        this.state.pics.length,
                        this.state.columns * this.state.rows + 1
                      );
                      t++
                    )
                      e.push(t);
                    return e;
                  },
                },
                {
                  key: 'componentDidMount',
                  value: function() {
                    var e = this;
                    this.mountKeypress(),
                      c.default.addEventListener(
                        'fullscreenchange',
                        function() {
                          e.setState({
                            isFullScreen: null !== c.default.fullscreenElement,
                          });
                        }
                      );
                  },
                },
                {
                  key: 'media',
                  value: function(t) {
                    var n = this.findPic(t % this.state.pics.length);
                    switch (n.type) {
                      case 'image':
                        return r.default.h(
                          s.default,
                          e({ key: n.id }, n, {
                            width: 100 / this.state.columns,
                            handleError: window.opener.main.handleError,
                            handleLoaded: window.opener.main.handleLoaded,
                            onClick: this.handleClick,
                          })
                        );
                      case 'video':
                        return r.default.h(
                          o.default,
                          e({ key: n.id }, n, {
                            volume: 0.5,
                            width: 100 / this.state.columns,
                            handleError: window.opener.main.handleError,
                            handleLoaded: window.opener.main.handleLoaded,
                            onClick: this.handleClick,
                          })
                        );
                      case 'impossibru':
                        return "I can't believe you've done this";
                    }
                  },
                },
                {
                  key: 'render',
                  value: function() {
                    var e = this;
                    return r.default.h(
                      'main',
                      { className: 'slidePopupVessel rows' + this.state.rows },
                      this.determineSlidesArray().map(function(t) {
                        return e.media(e.state.current + t);
                      }),
                      r.default.h(
                        'div',
                        { className: 'slideDimensionsControls' },
                        r.default.h(
                          'div',
                          {
                            className: 'SDC__row SDC__active',
                            onClick: this.setRows(!0),
                          },
                          '▲'
                        ),
                        r.default.h(
                          'div',
                          { className: 'SDC__row' },
                          r.default.h(
                            'div',
                            {
                              className: 'SDC__active',
                              onClick: this.setColumns(!1),
                            },
                            '◀'
                          ),
                          r.default.h(
                            'em',
                            null,
                            this.state.columns,
                            ' × ',
                            this.state.rows
                          ),
                          r.default.h(
                            'div',
                            {
                              className: 'SDC__active',
                              onClick: this.setColumns(!0),
                            },
                            '▶'
                          )
                        ),
                        r.default.h(
                          'div',
                          {
                            className: 'SDC__row SDC__active',
                            onClick: this.setRows(!1),
                          },
                          '▼'
                        )
                      )
                    );
                  },
                },
              ]),
              a
            );
          })();
        w &&
          r.default.render(
            r.default.h(y, {
              ref: function(e) {
                return (window.main = e);
              },
            }),
            w
          );
      },
      {
        preact: 9,
        '../components/video': 5,
        '../components/image': 6,
        '../helpers/fullScreen': 8,
        '../helpers/placeHolderImage': 7,
      },
    ],
  },
  {},
  [3]
);
