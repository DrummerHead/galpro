/** @jsx preact.h */
// @flow

import preact from 'preact';
import Video from '../components/video';
import Image from '../components/image';
import fullScreen from '../helpers/fullScreen';
import placeHolderImage from '../helpers/placeHolderImage';
import type { PicMeta } from '../main';

const root = document && document.getElementById('root');

const toggleFullScreen = ({ elem, goFullScreen }) => {
  goFullScreen
    ? fullScreen.requestFullscreen(elem)
    : fullScreen.exitFullscreen();
};

type MainProps = {};
type MainState = {
  pics: Array<PicMeta>,
  current: number,
  columns: number,
  rows: number,
  isFullScreen: boolean,
};
class Main extends preact.Component<MainProps, MainState> {
  state = {
    pics: [],
    current: 0,
    columns: 5,
    rows: 3,
    isFullScreen: false,
  };

  findPic = (id: number): PicMeta =>
    this.state.pics.find((pic: PicMeta): boolean => pic.id === id) ||
    placeHolderImage;

  determineSlidesArray(): Array<number> {
    let unpure = [];
    for (
      let i = 1;
      i <
      Math.min(
        this.state.pics.length,
        this.state.columns * this.state.rows + 1
      );
      i++
    ) {
      unpure.push(i);
    }
    return unpure;
  }

  keypress = (ev: KeyboardEvent) => {
    switch (ev.key) {
      case 'j':
      case 'ArrowRight':
      case 'ArrowDown':
        window.opener.main.forwards(true);
        break;

      case 'k':
      case 'ArrowUp':
      case 'ArrowLeft':
        window.opener.main.backwards(true);
        break;

      case 'l':
        this.setState(prevState => ({
          isFullScreen: !prevState.isFullScreen,
        }));
        toggleFullScreen({
          elem: root,
          goFullScreen: this.state.isFullScreen,
        });
        break;

      case 'f':
        window.opener.main.setState(prevState => ({
          showMetadata: !prevState.showMetadata,
        }));
        break;

      case 'e':
      case ' ':
        window.opener.main.autoGalleryToggle();
        break;

      case '-':
        window.opener.main.setState(prevState => ({
          autoAdvanceDelay: prevState.autoAdvanceDelay + 100,
          autoAdvanceNextLoadingHits: 0,
        }));
        break;

      case '+':
      case '=':
        window.opener.main.setState(prevState => ({
          autoAdvanceDelay:
            prevState.autoAdvanceDelay > 100
              ? prevState.autoAdvanceDelay - 100
              : 100,
          autoAdvanceNextLoadingHits: 0,
        }));
        break;

      case '§':
      case 'q':
        window.opener.main.setState(prevState => ({ debug: !prevState.debug }));
        break;

      case '±':
        window.opener.main.setState({ retryAmount: 0 });
        break;

      case '3':
        window.opener.main.handleError(
          window.opener.main.state.current,
          true
        )();
        break;
    }
  };

  mountKeypress = () => {
    document.addEventListener('keypress', this.keypress);
  };

  unmountKeypress = () => {
    document.removeEventListener('keypress', this.keypress);
  };

  componentDidMount() {
    this.mountKeypress();

    fullScreen.addEventListener('fullscreenchange', () => {
      this.setState({
        isFullScreen: fullScreen.fullscreenElement !== null,
      });
    });

    window.addEventListener('beforeunload', this.itClosed);
  }

  itClosed = () => {
    this.unmountKeypress();
    window.opener.main.setState({ slidePopOpen: false });
  };

  setPics = (pics: Array<PicMeta>) => {
    this.setState({ pics });
  };

  setCurrent = (current: number) => {
    this.setState({ current });
  };

  setColumns = (add: boolean) => ev => {
    this.setState(prevState => ({
      columns: add ? prevState.columns + 1 : prevState.columns - 1,
    }));
  };

  setRows = (add: boolean) => ev => {
    this.setState(prevState => ({
      rows: add ? prevState.rows + 1 : prevState.rows - 1,
    }));
  };

  handleClick = (id: number) => (ev: MouseEvent) => {
    const thisPic = this.findPic(id);
    window.opener.main.jumpTo(thisPic.id, true);
  };

  media(id) {
    const thisPic: PicMeta = this.findPic(id % this.state.pics.length);
    switch (thisPic.type) {
      case 'image':
        return (
          <Image
            key={thisPic.id}
            {...thisPic}
            width={100 / this.state.columns}
            handleError={window.opener.main.handleError}
            handleLoaded={window.opener.main.handleLoaded}
            onClick={this.handleClick}
          />
        );
      case 'video':
        return (
          <Video
            key={thisPic.id}
            {...thisPic}
            volume={0.5}
            width={100 / this.state.columns}
            handleError={window.opener.main.handleError}
            handleLoaded={window.opener.main.handleLoaded}
            onClick={this.handleClick}
          />
        );
      case 'impossibru':
        return "I can't believe you've done this";
    }
  }

  render() {
    return (
      <main className={`slidePopupVessel rows${this.state.rows}`}>
        {this.determineSlidesArray().map((num: number) =>
          this.media(this.state.current + num)
        )}
        <div className="slideDimensionsControls">
          <div className="SDC__row SDC__active" onClick={this.setRows(true)}>
            ▲
          </div>
          <div className="SDC__row">
            <div className="SDC__active" onClick={this.setColumns(false)}>
              ◀
            </div>
            <em>
              {this.state.columns} × {this.state.rows}
            </em>
            <div className="SDC__active" onClick={this.setColumns(true)}>
              ▶
            </div>
          </div>
          <div className="SDC__row SDC__active" onClick={this.setRows(false)}>
            ▼
          </div>
        </div>
      </main>
    );
  }
}

root && preact.render(<Main ref={main => (window.main = main)} />, root);
