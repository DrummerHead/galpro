// @flow

import type { ConfigType } from './main';

const config: ConfigType = {
  isAnumberedImgHref: true,
  prevChunk: 'http://madamevoyeur.com/images/2018-3/',
  postChunk: '.jpg',
  hasPadZeros: false,
  padZeros: 5,
  current: 18001,
  fullUrl: 'http://madamevoyeur.com/images/2018-3/18001.jpg',
};

window.config = config;
