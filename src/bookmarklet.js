// @flow

import type { ConfigType } from './main';
type PicType = 'image' | 'video' | 'null';
export type Pic = { href: string, type: PicType, title: string };

export var makeConfig = (
  prevChunk: string,
  current: string,
  postChunk: string,
  fullUrl?: string = `${prevChunk}${current}${postChunk}`
): ConfigType => ({
  isAnumberedImgHref:
    /\.(jpe?g|gif|png|webp|svg|webm|mp4|ogg)$/i.test(fullUrl) &&
    /\d+\.\w{3,4}$/i.test(fullUrl),
  prevChunk,
  postChunk,
  hasPadZeros: /^0/.test(current),
  padZeros: current.length,
  current: parseInt(current, 10),
  fullUrl,
});
var config = ((wlhref: string): ConfigType => {
  const sourceImgParts = wlhref.match(/^(.*?)(\d+)(\.\w{3,4})$/);
  const [_, prevChunk, currentStr, postChunk] = sourceImgParts
    ? sourceImgParts
    : ['h', wlhref, '001', '.extension'];

  return makeConfig(prevChunk, currentStr, postChunk, wlhref);
})(window.location.href);

// Return type of 'pic' according to extension
export var formatFinder = (href: string): PicType => {
  if (href.match(/\.(jpe?g|gif|png|webp|svg)$/i)) {
    return 'image';
  } else if (href.match(/\.(webm|mp4|ogg)$/i)) {
    return 'video';
  } else {
    return 'null';
  }
};

// Make silly imgur gifv "format" into mp4 link
var imgurTransform = (linkText: string): string =>
  linkText.match(/imgur\.com\/[^.]*\.gifv?/)
    ? linkText
        .replace(/^http:/, 'https:')
        .replace(/:\/\/imgur\.com/g, '://i.imgur.com')
        .replace(/\.gifv?/, '.mp4')
    : linkText;

// Make gfycat links into mp4 link
var gfycatTransform = (linkText: string): string =>
  linkText.match(/^https?:\/\/gfycat.com\//i)
    ? linkText
        .replace(/\/gifs\/detail\//, '/')
        .replace(
          /^(https?:\/\/)gfycat.com\/([^/]*)\/?$/,
          '$1giant.gfycat.com/$2.webm'
        )
    : linkText;

var linkTransforms = (linkText: string): string =>
  gfycatTransform(imgurTransform(linkText));

// All the links
var p: Array<Pic> = config.isAnumberedImgHref
  ? [
      {
        href: config.fullUrl,
        type: formatFinder(config.fullUrl),
        title: config.fullUrl,
      },
    ]
  : Array.from(document.links)
      .map((linkElem: HTMLElement): Pic => ({
        href: (linkElem.getAttribute('href') || '').replace(/\?.*$/, ''),
        title: linkElem.textContent,
        type: 'null',
      }))

      // Only those links that interest me
      .filter(
        (pic: Pic): boolean =>
          // has to be a media format (including bogus gifv format, to process later)
          (/\.(jpe?g|gif|png|webp|svg|webm|mp4|ogg|gifv)$/i.test(pic.href) ||
            // or at least a gfycat url
            /^https?:\/\/gfycat.com\//i.test(pic.href)) &&
          // and can't be an image like "apple-200x150.jpg"
          !/\d+x\d+\.\w{3,5}$/i.test(pic.href)
      )

      // Remove repeated links that follow each other
      .reduce(
        (acc: Array<Pic>, curr: Pic): Array<Pic> =>
          curr.href === acc[acc.length - 1].href ? acc : [...acc, curr],
        [{ href: '', title: '', type: 'null' }]
      )
      .slice(1)

      // transform links and add type
      .map((pic: Pic): Pic => {
        const href = linkTransforms(pic.href);
        return {
          ...pic,
          href,
          type: formatFinder(href),
        };
      });

console.info(`config`);
console.log(config);

console.info(`p`);
console.log(p);
