// @flow

import type { Keys } from './components/KeyboardOverlay';

// http://unicode.party/
// https://r12a.github.io/app-conversion/

export const KOCVideo: Keys = [
  {
    padLeft: 1,
    keys: [['u', '\u23EF'], ['i', '\u23EA'], ['o', '\u23E9']],
  },
  {
    padLeft: 0,
    keys: [
      ['h', '\uD83C\uDD95'],
      ['j', '\u2B07\uFE0F'],
      ['k', '\u2B06\uFE0F'],
      ['l', '\uD83D\uDDA5'],
    ],
  },
  {
    padLeft: 0,
    keys: [['n', '\uD83D\uDC22'], ['m', '\uD83D\uDC07'], [',', '\uD83D\uDD04']],
  },
];

export const KOCImage: Keys = [
  {
    padLeft: 0,
    keys: [
      ['h', '\uD83C\uDD95'],
      ['j', '\u2B07\uFE0F'],
      ['k', '\u2B06\uFE0F'],
      ['l', '\uD83D\uDDA5'],
    ],
  },
  {
    padLeft: 0,
    keys: [['n', '\u25BC'], ['m', '\u256C']],
  },
];

export const KOCGlobal: Keys = [
  {
    padLeft: 2,
    keys: [['e', '\uD83C\uDFC1'], ['r', '\u23EE']],
  },
  {
    padLeft: 0,
    keys: [
      ['a', '\uD83C\uDD0C'],
      ['s', '\uD83D\uDD01'],
      ['d', '\uD83C\uDF9E'],
      ['f', '\u2139\uFE0F'],
    ],
  },
];
