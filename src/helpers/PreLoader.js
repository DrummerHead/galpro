// @flow

import type { PicMeta } from '../main';

// TODO: Contemplate reversing the list of Pics

class PreLoader {
  pics: Array<PicMeta>;
  prevCurrent: number;
  current: number;
  delta: number;

  constructor(pics: Array<PicMeta>, delta: number) {
    this.pics = pics;
    this.delta = delta;
    this.prevCurrent = -delta;
  }

  loadImage(i: number, delta: number) {
    const proceed = () => {
      if (delta > 0) {
        this.loadImage(i + 1, delta - 1);
      }
    };

    if (this.pics[i]) {
      if (this.pics[i].type === 'image') {
        const image = new Image();
        image.src = this.pics[i].href;
        image.addEventListener('load', proceed);
        image.addEventListener('error', proceed);
      } else {
        proceed();
      }
    }
  }

  setCurrent(i: number) {
    this.current = i;

    if (Math.abs(this.current - this.prevCurrent) >= this.delta) {
      this.prevCurrent = i;
      this.loadImage(i, this.delta);
    }
  }
}

export default PreLoader;
