// @flow

import { addZeros } from './generateNumPics';

import type { ConfigType } from '../main';

const configParse = (config: ConfigType): ConfigType => {
  if (config.current > 9999) {
    const curr: string = config.hasPadZeros
      ? addZeros(String(config.current), config.padZeros)
      : String(config.current);

    const parts = curr.match(/^(\d*)(\d{4})$/);
    if (Array.isArray(parts)) {
      const newCurr = parseInt(parts[2], 10);
      const prevChunkPostfix = parts[1];
      const padZeros = parts[2].length;
      return {
        ...config,
        prevChunk: `${config.prevChunk}${prevChunkPostfix}`,
        current: newCurr,
        hasPadZeros: true,
        padZeros,
      };
    }
  }

  return config;
};

export default configParse;
