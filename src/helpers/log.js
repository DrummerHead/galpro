// @flow

const log = <T>(rtrn: T, message: string): T => {
  console.info(message);
  console.log(rtrn);
  return rtrn;
};

export default log;
