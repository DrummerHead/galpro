// @flow

import type { MainState } from '../main';

//        let displayGallery = false;
//        let lastDisplayGalleryWasAuto = false;
//
//        if (prevState.lastDisplayGalleryWasAuto) {
//          if (pic.hasError) {
//            displayGallery = true;
//            lastDisplayGalleryWasAuto = prevState.lastDisplayGalleryWasAuto; // true
//          } else {
//            displayGallery = false;
//            lastDisplayGalleryWasAuto = prevState.lastDisplayGalleryWasAuto; // true ; tricky one
//          }
//        } else {
//          if (pic.hasError) {
//            displayGallery = true;
//            lastDisplayGalleryWasAuto = true;
//          } else {
//            displayGallery = prevState.displayGallery; // previous
//            lastDisplayGalleryWasAuto = prevState.lastDisplayGalleryWasAuto; // false ; tricky
//          }
//        }
//
//        displayGallery = pic.hasError
//          ? true
//          : prevState.lastDisplayGalleryWasAuto
//            ? false
//            : prevState.displayGallery;
//        lastDisplayGalleryWasAuto =
//          pic.hasError && !prevState.lastDisplayGalleryWasAuto
//            ? true
//            : prevState.lastDisplayGalleryWasAuto;

const autoGallery = (
  hasError: boolean,
  prevState: MainState
): { displayGallery: boolean, lastDisplayGalleryWasAuto: boolean } => ({
  displayGallery: hasError
    ? true
    : prevState.lastDisplayGalleryWasAuto ? false : prevState.displayGallery,
  lastDisplayGalleryWasAuto:
    hasError && !prevState.lastDisplayGalleryWasAuto
      ? true
      : prevState.lastDisplayGalleryWasAuto,
});

export default autoGallery;
