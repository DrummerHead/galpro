// @flow

import type { Pic } from '../bookmarklet';
import type { PicMeta, ConfigType } from '../main';

export const addZeros = (n: string, padZeros: number): string =>
  n.length < padZeros ? addZeros(`0${n}`, padZeros) : n;

const genHref = (config: ConfigType, curr: number): string =>
  `${config.prevChunk}${
    config.hasPadZeros ? addZeros(curr.toString(), config.padZeros) : curr
  }${config.postChunk}`;

const generateNumPics = (
  imageList: Array<Pic>,
  config: ConfigType
): { pics: Array<PicMeta>, current: number } => {
  const arbitraryLargeNumber = 1000;
  let unpureArray = [];
  for (let i = 0; i <= config.current + arbitraryLargeNumber; i++) {
    unpureArray.push(i);
  }

  return {
    pics: unpureArray.map((curr: number) => ({
      href: genHref(config, curr),
      type: imageList[0].type,
      title: config.prevChunk,
      id: curr,
      isCurrent: curr === config.current,
      hasError: false,
      hasLoaded: false,
      isRetrying: false,
      retries: 0,
      duration: 0,
    })),
    current: config.current,
  };
};

export default generateNumPics;
