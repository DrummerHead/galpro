// TODO: Add flow and type annotate

const key = {
  fullscreenElement: 0,
  requestFullscreen: 1,
  exitFullscreen: 2,
  fullscreenchange: 3,
};

const webkit = [
  'webkitFullscreenElement',
  'webkitRequestFullscreen',
  'webkitExitFullscreen',
  'webkitfullscreenchange',
];

const moz = [
  'mozFullScreenElement',
  'mozRequestFullScreen',
  'mozCancelFullScreen',
  'mozfullscreenchange',
];

const ms = [
  'msFullscreenElement',
  'msRequestFullscreen',
  'msExitFullscreen',
  'MSFullscreenChange',
];

const vendor =
  ('fullscreenEnabled' in document && Object.keys(key)) ||
  (webkit[0] in document && webkit) ||
  (moz[0] in document && moz) ||
  (ms[0] in document && ms) ||
  [];

export default {
  requestFullscreen: element => element[vendor[key.requestFullscreen]](),
  requestFullscreenFunction: element => element[vendor[key.requestFullscreen]],
  get exitFullscreen() {
    return document[vendor[key.exitFullscreen]].bind(document);
  },
  addEventListener: (type, handler, options) =>
    document.addEventListener(vendor[key[type]], handler, options),
  removeEventListener: (type, handler, options) =>
    document.removeEventListener(vendor[key[type]], handler, options),
  get fullscreenElement() {
    return document[vendor[key.fullscreenElement]];
  },
};
