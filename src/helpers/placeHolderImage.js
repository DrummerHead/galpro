// @flow

import type { PicMeta } from '../main';

const placeHolderImage: PicMeta = {
  id: 404,
  href:
    'data:image/gif;base64,R0lGODlhAQABAIAAAMLCwgAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw==',
  type: 'image',
  title: 'Placeholder image',
  isCurrent: false,
  hasError: false,
  hasLoaded: true,
  isRetrying: false,
  retries: 0,
  duration: 0,
};

export default placeHolderImage;
