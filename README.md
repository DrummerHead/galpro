# galPro - on the fly gallery generator

This is a faaaantaaastic bookmarklet that does so many things I don't even have the ENERGY to post here all the stuff it does.

But what I _do_ need to document is how to "deploy" this thing because I just had to figure it out again after some time of not adding features, and yeah I need to document it.

# Installation

To "install" go [check this file](https://bitbucket.org/DrummerHead/galpro/src/master/dist/b.js), press the trhee dots to get to "Open raw", and now copy that code into the location of your bookmarklet.

# Features

TODO

# How to use

TODO

# Deployment

What you end up with is a uglified script that:

1. Creates a configuration object that among many things defines whether you're applying the bookmarklet to a numbered media or if you're applying it to a page full of links
2. Gets all the links in the current page
3. Creates a string that represents an html document, with the inlined configuration and list of images. This HTML has external links to fetch more CSS and JS that actually make the gallery run
4. document.write the previously mentioned string, rendering the current page into our beloved gallery

How do we get to this script?

After you're happy with the state of the application, run

```
yarn build
```

Which will give us two important files at `./dist`: The main CSS file and the main JS file. There will also be two more files for the mock config and list files, but we don't want those. Figure out which these two files are and commit & push.

Now go check the raw version of these two files over at [bitbucket](https://bitbucket.org/DrummerHead/galpro/src/master/dist/), paste those links into http://raw.githack.com/ and get the rawcdn links that can be used by our bookmarklet. Save this links, you will use them.

If you made changes to the bookmarklet script, you should also run:

```
yarn bm
```

which removes flow, prettifies and copies the bookmarklet to `./manual` folder.

Now go and edit `./manual/bookmarklet.js` and `./manual/reference.js`. You have to replace the bottom code of `bookmarlet.js` as `reference.js` says and also put the right URL in `cssPath` and `jsPath` variables. Also check `src/slidePopup/index.html` and update link stylesheet href.

After the file is ready, run:

```
yarn min
```

To minify `bookmarlet.js`, add `javascript:(function(){})();` and put this as `./dist/b.js` which is finally the bookmarklet.

Now commit and push again so we can have this bookmarklet accessible online. Love you man.

