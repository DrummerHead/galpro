# improvements

## To Do


- [ ] Quizas contemplar qué pasa si no hay ningun link a una imagen en el documento y no hijack todo el dom :)
- [ ] Show keyboard overlay on mouse move (not only on hover exact thingy)
- [ ] Instead of loading 100 images to the future and start again when you reach x number, load 30 images into the future in relation to current image always
- [ ] autoAdvance behave differently for videos. Estaba pensando en un "min time" de viewing configurable, y entonces la cantidad de veces que vos miras es n loops tal que veas al menos min time amount. Si el mintime es 10 segundos y hay un video de 7 segundos, son dos loops. Si el video es de 14 segundos, es un loop. Entendeste amegue. Ahí voy a tener que hookearme a eventos de repetición de veces, duración total del video y más magias. Se va a ir al carajo un poco creo :P
- [ ] 
- [ ] 

### Low priority (nice to have but meh)

- [ ] Research on image paint time. There's some stuttering when passing from one image to another, and the content is already fetched; so it must be image rendering time.
- [ ] hay links de imgur a single pictures que linkean a la pagina en vez de la raw image, puedo procesar esos tambien
- [ ] 




## Done

- [x] have a "keep looking forward until you find an image that actually loads" feature... or perhaps don't even show error images and skip forward? I have to thing about this
- [x] Some sort of autoplay (that you can control the time delta) (that waits until next image is loaded)
- [x] When galnumming, if the image name is like http://rap.to/images/full/47/320/320688159.jpg I can't make a 320688159 item long array. Cap that to a 9999 item long array or something and don't traverse all the images 'cause that ends up being too much to process.
- [x] Replicate "loading cache size augmentation on current loading" for videos too; this means hearing when a video is still loading when you're currently on it, which I'm currently not doing
- [x] double click on image does the m thing
- [x] 'r' (go to beginning) also triggers gallery mode
- [x] cache of images will prefetch future images up to a certain point (even beyond "naturally" cached gallery bottom)
- [x] Be able to reverse the list, and perhaps have "left" controls for different states (like show metadata and reverse list)
- [x] Be able to change the id of current image via clicking counter
- [x] Highlight numbers when reaching end of list
- [x] Add keyboard overlay for f and d (show metadata, trigger gallery mode)
- [x] Check animation of loading on currentpic, looks like not working, probably css selector
- [x] Loading translucid, so I can also see the progressive loading of the image
- [x] "Gallery mode" so I can see all the images available and click on anyone that I want to see it fullscreen. This... might involve caching more images... I don't know... I envision a grid with many items.
- [x] Error with previous cache image when webm and first image. I think that having the placeholder have the id of 0 is a problem
- [x] Dynamic cache number... perhaps increase cache images if there's too many occurences of showing loading to current image. That's an interesting idea.
- [x] sometimes if I scroll faster than images load, I end up seeing images I already saw. Would be ideal to hook to a image load event and show a lader until image comes or starts coming or something so that doesn't happen (so I don't see the previously loaded image in the element or something like that)
- [x] be able to select text from metadata overlay
- [x] meta overlay lo jode al hover image scroll magia cuando pasa por metaoverlay
- [x] mstrar en algun overlay on hover el href y title de la imagen. Esto involucraría obtener el texto del link cuando se fetchean todos los datos...
- [x] Button to "zoom" an image; make it width fill. then another button to slowly scroll down automatically ;) even better, tie mouse y mouse position to percentage thingy of image centering thingie when in that state, to scroll on mouse move
- [x] Account for errors... an image didn't load, the text appears. Make it big and centered and stuff.
- [x] sacar los `?` query parameters de las urls antes de analizarlas para ver si son imagenes
- [x] Mantener "consciencia" del estado de fullscreen. Agregar event listeners asi la app updatea su estado interno y no tengo que andar clickeando el boton de fullscreen dos veces al pedo
- [x] title replace
- [x] indicar cuando una imagen es la ultima
- [x] shortcut para entrar y salir de fullscreen (no solamente esc para salir)
- [x] overlay con teclas asi puedo hacer con mouse todo lo que puedo hacer con teclado
- [x] diferenciar imagenes de videos. visualmente, de alguna forma.
- [x] m'as cache (5 imgs)
- [x] add speedup - speeddown controls
- [x] https://gfycat.com/ analiziz?
- [ ] pausing a video, the playing again makes all videos play, see if can reproduce (can't reproduce)

