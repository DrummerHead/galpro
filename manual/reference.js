// In bookmarklet.js replace

console.info(`config`);
console.log(config);

console.info(`p`);
console.log(p);

// with:

const picstring = JSON.stringify(p);
const configString = JSON.stringify(config)

const cssPath = 'https://bbcdn.githack.com/DrummerHead/galpro/raw/3c1f799236135c33fd70b5b941c956720865d877/dist/057b554f65370217adf0214f595f7e86.css';
const jsPath = 'https://bbcdn.githack.com/DrummerHead/galpro/raw/3c1f799236135c33fd70b5b941c956720865d877/dist/58604a081c97af3e58e303918870081f.js';

const html = `<!doctype html><html> <head> <meta charset="utf-8"> <title>&#128139; ${document.title}</title> <link href="${cssPath}" rel="stylesheet"> </head> <body> <div id="root"></div> <script>window.pics = ${picstring}</script> <script>window.config = ${configString}</script> <script src="${jsPath}"></script> </body> </html>`

// window.open('data:text/html,' + encodeURIComponent(html));
document.write(html);
document.close();


// Making sure that you also replace cssPath and jsPath with the right files URL
// Be careful to remove any "export" that might be left in the bookmarklet code
