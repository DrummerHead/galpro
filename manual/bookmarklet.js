var makeConfig = (
  prevChunk,
  current,
  postChunk,
  fullUrl = `${prevChunk}${current}${postChunk}`
) => ({
  isAnumberedImgHref:
    /\.(jpe?g|gif|png|webp|svg|webm|mp4|ogg)$/i.test(fullUrl) &&
    /\d+\.\w{3,4}$/i.test(fullUrl),
  prevChunk,
  postChunk,
  hasPadZeros: /^0/.test(current),
  padZeros: current.length,
  current: parseInt(current, 10),
  fullUrl,
});
var config = ((wlhref) => {
  const sourceImgParts = wlhref.match(/^(.*?)(\d+)(\.\w{3,4})$/);
  const [_, prevChunk, currentStr, postChunk] = sourceImgParts
    ? sourceImgParts
    : ['h', wlhref, '001', '.extension'];

  return makeConfig(prevChunk, currentStr, postChunk, wlhref);
})(window.location.href);

// Return type of 'pic' according to extension
var formatFinder = (href) => {
  if (href.match(/\.(jpe?g|gif|png|webp|svg)$/i)) {
    return 'image';
  } else if (href.match(/\.(webm|mp4|ogg)$/i)) {
    return 'video';
  } else {
    return 'null';
  }
};

// Make silly imgur gifv "format" into mp4 link
var imgurTransform = (linkText) =>
  linkText.match(/imgur\.com\/[^.]*\.gifv?/)
    ? linkText
        .replace(/^http:/, 'https:')
        .replace(/:\/\/imgur\.com/g, '://i.imgur.com')
        .replace(/\.gifv?/, '.mp4')
    : linkText;

// Make gfycat links into mp4 link
var gfycatTransform = (linkText) =>
  linkText.match(/^https?:\/\/gfycat.com\//i)
    ? linkText
        .replace(/\/gifs\/detail\//, '/')
        .replace(
          /^(https?:\/\/)gfycat.com\/([^/]*)\/?$/,
          '$1giant.gfycat.com/$2.webm'
        )
    : linkText;

var linkTransforms = (linkText) =>
  gfycatTransform(imgurTransform(linkText));

// All the links
var p = config.isAnumberedImgHref
  ? [
      {
        href: config.fullUrl,
        type: formatFinder(config.fullUrl),
        title: config.fullUrl,
      },
    ]
  : Array.from(document.links)
      .map((linkElem) => ({
        href: (linkElem.getAttribute('href') || '').replace(/\?.*$/, ''),
        title: linkElem.textContent,
        type: 'null',
      }))

      // Only those links that interest me
      .filter(
        (pic) =>
          // has to be a media format (including bogus gifv format, to process later)
          (/\.(jpe?g|gif|png|webp|svg|webm|mp4|ogg|gifv)$/i.test(pic.href) ||
            // or at least a gfycat url
            /^https?:\/\/gfycat.com\//i.test(pic.href)) &&
          // and can't be an image like "apple-200x150.jpg"
          !/\d+x\d+\.\w{3,5}$/i.test(pic.href)
      )

      // Remove repeated links that follow each other
      .reduce(
        (acc, curr) =>
          curr.href === acc[acc.length - 1].href ? acc : [...acc, curr],
        [{ href: '', title: '', type: 'null' }]
      )
      .slice(1)

      // transform links and add type
      .map((pic) => {
        const href = linkTransforms(pic.href);
        return {
          ...pic,
          href,
          type: formatFinder(href),
        };
      });

const picstring = JSON.stringify(p);
const configString = JSON.stringify(config)

const cssPath = 'https://bbcdn.githack.com/DrummerHead/galpro/raw/3c1f799236135c33fd70b5b941c956720865d877/dist/057b554f65370217adf0214f595f7e86.css';
const jsPath = 'https://bbcdn.githack.com/DrummerHead/galpro/raw/3c1f799236135c33fd70b5b941c956720865d877/dist/58604a081c97af3e58e303918870081f.js';

const html = `<!doctype html><html> <head> <meta charset="utf-8"> <title>&#128139; ${document.title}</title> <link href="${cssPath}" rel="stylesheet"> </head> <body> <div id="root"></div> <script>window.pics = ${picstring}</script> <script>window.config = ${configString}</script> <script src="${jsPath}"></script> </body> </html>`

document.write(html);
document.close();
